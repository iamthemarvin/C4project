package business;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class C4ServerSession {
	private static final int BUFSIZE = 32;
	private Socket socket;
	private boolean gameover;
	
	public C4ServerSession(){
		
	}
	
	
	public C4ServerSession(Socket socket) throws IOException{
		this.socket=socket;
		this.gameover=false;
		connection();
	}
	
	public void connection() throws IOException{
		int recvMsgSize;						// Size of received message
	    byte[] byteBuffer = new byte[BUFSIZE];	// Receive buffer

		// Run forever, accepting and servicing connections
	    
	      System.out.println("Handling client at " +
	    		  socket.getInetAddress().getHostAddress() + " on port " +
	    		  socket.getPort());
	      System.out.println("the message is here" + byteBuffer);

	      InputStream in = socket.getInputStream();
	      OutputStream out = socket.getOutputStream();

	      // Receive until client closes connection, indicated by -1 return
	      while ((recvMsgSize = in.read(byteBuffer)) != -1)
	        out.write(byteBuffer, 0, recvMsgSize);

	      						// Close the socket. This client is finished.
	 
	}
	
	
}
