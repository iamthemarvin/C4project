package business;

import java.net.*; // for Socket, ServerSocket, and InetAddress
import java.io.*; // for IOException and Input/OutputStream

public class C4Server {
	private final int SERVPORT = 50000;
	private ServerSocket servSock;

	public C4Server() throws IOException {
		this.servSock = new ServerSocket(SERVPORT);
		createSession();
	}

	public int getServPort() {
		return SERVPORT;
	}
	
	private void createSession() throws IOException {

		// Run forever, accepting and servicing connections
		for (;;)
		{
			Socket clntSock = servSock.accept(); // Get client connection
			C4ServerSession c4s = new C4ServerSession(clntSock);
			clntSock.close();
		}
		/* NOT REACHED */
	}
	
	
}
